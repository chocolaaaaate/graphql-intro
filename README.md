# GraphQL Server

My first GraphQL server, as a layer over the placeholder REST API freely available at https://jsonplaceholder.typicode.com.

Uses graphql-yoga and axios.

Run `nodemon` to start. 

Sumedh Kanade
