/**
 * My first GraphQL server, as a layer over the placeholder REST API freely available at https://jsonplaceholder.typicode.com.
 *
 * Sumedh Kanade
 * July 2020
 */

const { GraphQLServer } = require('graphql-yoga');
const axios = require('axios');

const BASE_API_URL = 'https://jsonplaceholder.typicode.com';

const schema = `
    type Query {
        about: String!
        users: [User!]!
        user(id: ID!): User
        posts(userId: ID!): [Post!]!
        post(id: ID!): Post
    }

    type User {
        id: ID!
        name: String!
        username: String!
        email: String!
        phone: String!
        website: String!
    }

    type Post {
        userId: ID!
        id: ID!
        title: String!
        body: String!
        comments: [Comment!]!
    }

    type Comment {
        postId: ID!
        id: ID!
        name: String!
        email: String!
        body: String!
    }
`;

const resolvers = {
  Query: {
    about: () =>
      'Sample GraphQL API layer for the REST API at https://jsonplaceholder.typicode.com/',
    users: () =>
      axios
        .get(`${BASE_API_URL}/users`)
        .then((res) => res.data)
        .catch((err) => console.error(`Error while fetching all users`, err)),
    user: (_, { id }) => {
      return axios
        .get(`${BASE_API_URL}/users/${id}`)
        .then((res) => res.data)
        .catch((err) =>
          console.error(`Error while fetching user (ID = ${id})`, err)
        );
    },
    posts: (_, { userId }) => {
      return axios
        .get(`${BASE_API_URL}/posts?userId=${userId}`)
        .then((res) => {
          const posts = res.data;
          posts.forEach((post) => {
            post.comments = axios
              .get(`${BASE_API_URL}/posts/${post.id}/comments`)
              .then((res) => res.data)
              .catch((err) =>
                console.error(
                  `Error while fetching comments for post (ID = ${post.id})`,
                  err
                )
              );
          });
          return posts;
        })
        .catch((err) =>
          console.error(
            `Error while fetching posts for user (user ID = ${userId})`,
            err
          )
        );
    },
    post: (_, { id }) => {
      return axios
        .get(`${BASE_API_URL}/posts/${id}`)
        .then((res) => {
          const post = res.data;
          post.comments = axios
            .get(`${BASE_API_URL}/posts/${id}/comments`)
            .then((res) => res.data)
            .catch((err) =>
              console.error(
                `Error while fetching comments for post (ID = ${id})`,
                err
              )
            );
          return post;
        })
        .catch((er) =>
          console.error(`Error while fetching post (ID = ${id})`, er)
        );
    },
  },
};

const graphqlServer = new GraphQLServer({
  typeDefs: schema,
  resolvers: resolvers,
});

graphqlServer.start(
  {
    port: 9090,
    endpoint: '/graphql',
  },
  () => {
    console.log(`GraphQL server started.`);
  }
);
